Module 2 –  Cloud Computing Landscape and Ecosystem
======================================================

This document captures the draft design of Module 2

Learning Outcomes

At the end of the module, the participants will be able to:


    * understand the various types of cloud ecosystems
    * create hybrid cloud using AWS, Azure, and GCP
    * work on containers using VMs, docker, and kubernetes

   
Duration
    3 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi
    * 3 Quizzes (graded)
    * 3 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 6, 14, 20, 40

  "Week1", "Lecture","Public Cloud Ecosystem ", "AWS, Azure, Google Cloud , IBM Cloud Services, Alibaba"
         ,          ,"Private Cloud Ecosystem", "VMware, Microsoft, Red Hat,OpenStack"
         , "Labs",                        , "Overview of Cloud Ecosystem, Overview of Private Cloud Ecosystem"
  "Week2", "Lecture","Hybrid Cloud", "Overview of Hybrid Cloud , Hybrid Cloud with AWS, Hybrid Cloud with Azure, Hybrid Cloud with Google Cloud"
         , "Labs",          , "Creating Hybrid Cloud with AWS, Azure and Google cloud"
  "Week3", "Lecture","Overview of Containers", "What is a Container?, VMs vs. Containers, Docker , Kubernetes, Containers as a Service (CaaS), Benefits of Containers, Containers in the Cloud"
         ,          ,"Serverless Computing", "What is Serverless?, Functions as a Service (FaaS), FaaS vs IaaS, FaaS vs PaaS, FaaS vs CaaS, Benefits of FaaS"
         , "Labs",          , "Creating containers using VMware and Docker, Practicals on function as a service"


