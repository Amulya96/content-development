Module 6(c) – Google Cloud Services, Deploying and Managing Google Cloud Resources
====================================================================================

This document captures the draft design of Module 6(c)

Learning Outcomes

At the end of the module, the participants will be able to

    * understand various cloud services managed by AWS 
    * deploy and manage Google Cloud resources


   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 6, 14, 25, 40

  "Week1", "Lecture", "Messaging", "Pub/Sub"
         ,          , "Big Data & Analytics", "BigQuery, DataProc, Datafow, Looker"
         , "Labs",     ,"T B D"
  "Week2", "Lecture", "Internet of Things", "Cloud IoT Core"
         ,          , "Machine Learning and AI", "Cloud AI APIs, Cloud AI Platform"
         , "Labs",                        , "T B D"
  "Week3", "Lecture", "Infrastructure as Code", "Deployment Manager, Terraform, Google Cloud SDK, Use Cases"
         , "Labs",    ,"T B D"
  "Week4", "Lecture", "Automating Application Deployment", "Cloud Source Repo, Cloud Build, Cloud Code"
         , "Labs",   ,"T B D"