Module 3 – Core Building Blocks of the Cloud
==============================================

This document captures the draft design of Module 3


Learning Outcomes

At the end of the module, the participants will be able to:

    * implement cloud computing services using Virtual Machines
    * understand insights of storage, network, database, security, and management services

   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 8, 14, 20, 40

  "Week1, 2", "Lecture", "Compute Services", "Virtual Machines, Containers, Container Clusters, Functions"
         ,          , "Storage Services", "Object Storage, Block Storage, File Storage"
         , "Labs",                        , "Working on virtual machines for creating container clusters, object storage, block storage, and file storage"
  "Week3, 4", "Lecture", "Network", "IP Addresses, Load Balancer, Firewall, Virtual Private Cloud, Hybrid Connectivity"
         ,          , "Databases", "Relational , NoSQL, In-Memory, Graph, Time Series"
         ,          , "Security & Management", "Identity Management, Secrets, Encryption"
         , "Labs",          , "Practical on VMware for creating IP addresses, load balancer, hybrid connectivity, graph, and time series."

