Module 6(b) – Azure Managed Services, Deploying and Managing Azure Resources
==============================================================================

This document captures the draft design of Module 6(b)

Learning Outcomes

At the end of the module, the participants will be able to

    * understand various cloud services managed by AWS 
    * deploy and manage Azure resources


   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 6, 14, 25, 40

  "Week1", "Lecture", "Messaging", "Azure Queues, Azure Service Bus, Azure EventGrid"
         ,          , "Big Data & Analytics", "HDInsight, EventHub, SQL Data Warehouse, Azure Synapse, PowerBI"
         , "Labs",     ,"T B D"
  "Week2", "Lecture", "Internet of Things", "Azure IoT, Azure IoT Central, Azure IoT Edge"
         ,          , "Machine Learning and AI", "Azure Cognitive Services, Azure ML"
         , "Labs",                        , "T B D"
  "Week3", "Lecture", "Infrastructure as Code", "Azure Resource Manager, Terraform, Azure SDKs, Use cases"
         , "Labs",    ,"T B D"
  "Week4", "Lecture", "Automating Application Deployment", "GitHub Actions, Azure DevOps, Azure Container Services"
         , "Labs",  ,"T B D"