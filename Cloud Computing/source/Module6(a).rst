Module 6(a) – AWS Managed Services, Deploying and Managing AWS Resources
==========================================================================

This document captures the draft design of Module 6(a)

Learning Outcomes

At the end of the module, the participants will be able to

    * understand various cloud services managed by AWS 
    * deploy and manage AWS resources


   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 6, 14, 25, 40

  "Week1", "Lecture", "Messaging", "Amazon SQS, Amazon SNS, Amazon MQ"
         ,          , "Big Data & Analytics", "Amazon RedShift, Amazon Kinesis, Amazon EMR, Amazon QuickSight"
         , "Labs",     ,"T B D"
  "Week2", "Lecture", "Internet of Things", "AWS IoT Core, AWS Sitewise, AWS Greengrass"
         ,          , "Machine Learning and AI", "Amazon AI Services, Amazon SageMaker"
         , "Labs",                        , "T B D"
  "Week3", "Lecture", "Infrastructure as Code", "AWS CloudFormation, Terraform, Amazon CDK, Use Cases"
         , "Labs",   ,"T B D"
  "Week4", "Lecture", "Automating Application Deployment", "AWS CodeCommit, AWS CodeDeploy, AWS CodePipeline"
         , "Labs",   ,"T B D"