Module 7 – Understanding Hybrid Cloud and Multi-Cloud
=======================================================

This document captures the draft design of Module 7

Learning Outcomes

At the end of the module, the participants will be able to:

* work on real-world hybrid and multi-cloud case studies with VMware, AWS, Azure, and GCP.

   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 2 Quizzes (graded)
    * 2 Assignments (graded)
    * 2 Case Studies (graded)
    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 6, 14, 20, 40

  "Week1", "Lecture", "Hybrid Cloud", "Overview of Hybrid Cloud, Business Scenarios"
         ,          , "Multi-Cloud ", "Why Multi-Cloud, Business Scenarios, Case Study"
         , "Labs",     ,"T B D"
  "Week2", "Lecture", "Implementing Hybrid Cloud ", "VMware on AWS, VMware on Azure, VMware on Google Cloud, AWS Outposts, Azure Stack, Google Anthos"
         , "Labs",                        , "T B D"
  "Week3", "Lecture", "Implementing Multi-Cloud", "Amazon EKS Anywhere, Azure Arc, Google Anthos for AWS /Azure"
         , "Labs",    ,"T B D"
  "Week4", "Labs",   ,"T B D"



