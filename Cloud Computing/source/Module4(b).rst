Module 4(b) – Microsoft Azure Building Blocks
===============================================

This document captures the draft design of Module 4(b)

Learning Outcomes

At the end of the module, the participants will be able to:

    * understand about various building blocks of Azure, including compute, storage, network, databases, security, and management


   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 8, 14, 20, 40

  "Week1, 2", "Lecture", "Big Picture of Azure", "Regions,Availability Zones, Resource Billing, Azure Subscriptions"
         ,          , "Compute", "Azure VMs,Azure App Service, Azure Functions, Azure Kubernetes Service"
         ,          , "Storage", "Azure Storage, Azure CDN, Azure Backup, Azure Disks, Amazon Files"
         , "Labs",                        , "Overview of availability zones,resource billing, and identity management, Overview of  Azure compute services, Overview Azure storage services  "
  "Week3, 4", "Lecture", "Network", "Static IP, Load Balancer, Security Groups, Virtual Networks, Express Route"
         ,          , "Databases", "Azure SQL, Amazon CosmosDB, Amazon Cache, Azure OSS Databases"
         ,          , "Security & Management", "Security Center, Azure Active Directory,Azure Sentinel "
         , "Labs",          , "Implement network, database, security, and management concepts using Azure."
