Module 4(a) – AWS Building Blocks
===================================

This document captures the draft design of Module 4(a)

Learning Outcomes

At the end of the module, the participants will be able to:

    * understand about various building blocks of AWS, including compute, storage, network, databases, security, and management


   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 8, 14, 20, 40

  "Week1, 2", "Lecture", "Big Picture of AWS", "Regions, Availability Zones, Resource Billing, Identity Management"
         ,          , "Compute", "Amazon EC2, Amazon EKS, AWS Lambda"
         ,          , "Storage", "Amazon S3, Amazon CloudFront, Amazon Glacier, Amazon EBS, Amazon EFS"
         ,          , "Core Attributes of Cloud", "Programmability, Scalability, Automation, Self Service, Pay as you Go"
         , "Labs",                        , "Overview of availability zones,resource billing, and identity management, Overview of  AWS compute services, Overview AWS storage services"
  "Week3, 4", "Lecture", "Network", "Elastic IP, ELB/ALB, Security Groups, Amazon VPC, Amazon Direct Connect"
         ,          , "Databases", "Amazon RDS / Aurora, Amazon DynamoDB, Amazon ElastiCache, Amazon Neptune, Amazon Timestream"
         ,          , "Security & Management", "AWS IAM, Amazon KMS, Data Encryption in S3"
         , "Labs",          , "Implement network, database, security, and management concepts using AWS."

