Module 4(c) – Google Cloud Building Blocks
============================================

This document captures the draft design of Module 4(c)

Learning Outcomes

At the end of the module, the participants will be able to:

    * understand about various building blocks of GCP, including compute, storage, network, databases, security, and management


   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 8, 14, 20, 40

  "Week1, 2", "Lecture","Big Picture of Google Cloud", "Regions, Availability Zones, Resource Billing, Identity Management"
         ,          , "Compute", "Compute Engine, App Engine, Cloud Functions, Kubernetes Engine"
         ,          , "Storage", "Cloud Storage, Cloud CDN, Persistent Disks, Cloud Filestore"
         , "Labs",                        , "Overview of availability zones,resource billing, and identity management, Overview of  GCP compute services, Overview GCP storage services"
  "Week3, 4", "Lecture", "Network", "Static IP, Load Balancer, Security Groups, Virtual Networks, Peering"
         ,          , "Databases", "Cloud SQL, Bigtable, Memorystore, Cloud Spanner"
         ,          , "Security & Management ", "Cloud IAM, Cloud Armor, Secret Manager, Cloud Key Management"
         , "Labs",          , "Implement network, database, security, and management concepts using GCP"

