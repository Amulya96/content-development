Module 5 - Managed Cloud Services
===================================

This document captures the draft design of Module 5

Learning Outcomes

At the end of the module, the participants will be able to:

    * understand about different managed cloud services such as Big Data & Analytics, IoT, Machine Learning, and AI.



   
Duration
    4 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi 
    * 4 Quizzes (graded)
    * 4 Assignments (graded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 8, 14, 20, 40

  "Week1, 2", "Lecture", "Messaging", "Message Queue, Notifications, Message Broker"
         ,          , "Big Data & Analytics", "Data Warehouse, Data Lake,,Data Ingestion, Map Reduce, Data Visualization / Business Intelligence"
  "Week3, 4", "Lecture", "Internet of Things", "Device Registry, Device Security, Device Communication, Edge Manager"
         ,          , "Machine Learning and AI", "Cognitive Computing APIs, ML PaaS, Automated ML"
