==============================
Welcome to Cloud Computing !
==============================

This document captures the proposed design of the **PG Diploma in Cloud Computing** program to be jointly offered by IIIT Delhi and TalentSprint.


Target Audience
    1. Graduates keen to have a career in Cloud computing.
    2. Working professionals who want to master latest Cloud technologies.


Eligibility
    * Require minimum bachelors degree (B.Sc computers, BCA, BE, B.Tech) or equivalent.
 

Selection Procedure
    * Participants to submit SOP document.
    * IIIT Delhi PG Diploma eligibility rules to be applied.


Learning Outcomes
    1. Learn how to prepare, develop, and deploy applications using the best Cloud Computing services.
    2. Get hands-on experience in industry-oriented projects.


Delivery Mode
    * Hybrid model: Combination of Live Online Classes with a campus visit.


Proposed Participant's Workload
    * 4.5 hours of lecture every week.
    * 4 hours of lab every week.
    * 0.5 hours Quiz and Quiz explanations every week.
    * 5 hours self study every week.
   


PROGRAM DESIGN OPTION 1
+++++++++++++++++++++++++++

Includes 5 Core Modules + all 3 specializations (Azure, AWS, GCP)


  Duration 

    * 49 weeks (9 hours per week)
    * 12 months
    * About 441 hours + 245 hours of self study

  Program Structure

    * Two terms, two campus visits and one capstone project.
    * First campus visit will be after term 1, second campus visit will be after term 2 and capstone project.
    * Modular Exams are planned after each term.
    * Capstone project is planned after term 2.   
   
    +-------------------+--------------+--------------------------------------+
    + Course Structure  + Duration     +  Course Plan                         +
    +-------------------+--------------+--------------------------------------+
    + Term 1            + 21 Weeks     + Module 1, 2, 3, 4(a), 4(b), 4(c)     +
    +-------------------+--------------+--------------------------------------+
    + Campus visit      +  1 week      + TBD                                  +
    +-------------------+--------------+--------------------------------------+
    + Term 2            + 20 Weeks     + Module 5, 6(a), 6(b), 6(c), 7        +
    +-------------------+--------------+--------------------------------------+
    + Project           +  6 weeks     + Capstone Project                     +
    +-------------------+--------------+--------------------------------------+
    + Campus visit      +  1 week      + Project Presentation                 +
    +-------------------+--------------+--------------------------------------+


  Module Level Details

    +------------------+------------+-----------------------------------------------------------------------+
    + Module           + Duration   +   Topics Covered                                                      +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 1         +  2 weeks   + Overview of Cloud Services                                            +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 2         +  3 weeks   + Cloud Computing Landscape and Ecosystem                               +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 3         +  4 weeks   + Core Building Blocks of the Cloud                                     +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 4(a)      +  4 weeks   + AWS Building Blocks                                                   +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 4(b)      +  4 weeks   + Microsoft Azure Building Blocks                                       +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 4(c)      +  4 weeks   + Google Cloud Building Blocks                                          +
    +------------------+------------+-----------------------------------------------------------------------+
    + Campus visit_1   +  1 week    + TBD                                                                   +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 5         +  4 weeks   + Managed Cloud Services                                                +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 6(a)      +  4 weeks   + AWS Managed Services, Deploying and Managing AWS Resources            +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 6(b)      +  4 weeks   + Azure Managed Services, Deploying and Managing Azure Resources        +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 6(c)      +  4 weeks   + Google Cloud Services, Deploying and Managing Google Cloud Resources  +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 7         +  4 weeks   + Understanding Hybrid Cloud and Multi-Cloud                            +
    +------------------+------------+-----------------------------------------------------------------------+
    + Project          +  6 weeks   + Capstone Project (TBD)                                                +
    +------------------+------------+-----------------------------------------------------------------------+
    + Campus visit_2   +  1 week    + Project Presentation                                                  +
    +------------------+------------+-----------------------------------------------------------------------+



  Assessment and Grading

    Attendance
    * Minimum 75% of all sessions
    * Only those who attend ≥ 75% of session duration will be awarded attendance

    Evaluation will be based on the following components:
    * Quizzes
    * Assignments
    * Case Studies
    * Modular Exams 
    * Capstone Project

  .. csv-table::
    :header: "Assessment Type", "Total Count", "Best", "Points/Assessment", "Total points", "Weightage"
    :widths: 10, 5, 5, 10, 5, 5

    "Quizzes", 39, 35, 4, 140, 14%
    "Assignments", 43, 40, 5, 200, 20%
    "Case Studies", 2, 2, 30, 60, 6%
    "Modular Exams", 2, 2, 150, 300, 30%
    "Capstone", 1, 1, 300, 300, 30%
       , ,  , "Total", 1000, 100% 
    


PROGRAM DESIGN OPTION 2
++++++++++++++++++++++++++

Includes 5 core modules and any 1 specialization out of 3 mentioned below: 

1. Amazon Web Services
2. Microsoft Azure
3. Google Cloud Platform


  Duration 

    * 35 weeks (9 hours per week)
    * 8.5 months
    * About 315 hours + 175 hours of self study

  Program Structure

    * Two terms, two campus visits and one capstone project.
    * First campus visit will be after term 1, second campus visit will be after term 2 and capstone project.
    * Modular Exams are planned after each term.
    * Capstone project is planned after term 2.


    +-------------------+--------------+--------------------------------------+
    + Course Structure  + Duration     +  Course Plan                         +
    +-------------------+--------------+--------------------------------------+
    + Term 1            + 13 Weeks     + Module 1, 2, 3, 4(a)/4(b)/4(c)       +
    +-------------------+--------------+--------------------------------------+
    + Campus visit      +  1 week      + TBD                                  +
    +-------------------+--------------+--------------------------------------+
    + Term 2            + 12 Weeks     + Module 5, 6(a)/6(b)/6(c), 7          +
    +-------------------+--------------+--------------------------------------+
    + Project           +  8 weeks     + Capstone Project                     +
    +-------------------+--------------+--------------------------------------+
    + Campus visit      +  1 week      + Project Presentation                 +
    +-------------------+--------------+--------------------------------------+


  Module Level Details

    +------------------+------------+-----------------------------------------------------------------------+
    + Module           + Duration   +   Topics Covered                                                      +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 1         +  2 weeks   + Overview of Cloud Services                                            +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 2         +  3 weeks   + Cloud Computing Landscape and Ecosystem                               +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 3         +  4 weeks   + Core Building Blocks of the Cloud                                     +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 5         +  4 weeks   + Managed Cloud Services                                                +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 7         +  4 weeks   + Understanding Hybrid Cloud and Multi-Cloud                            +
    +------------------+------------+-----------------------------------------------------------------------+

    
    Specialization 1: Amazon Web Services

    +------------------+------------+-----------------------------------------------------------------------+
    + Module 4(a)      +  4 weeks   + AWS Building Blocks                                                   +
    +------------------+------------+-----------------------------------------------------------------------+
    + Campus visit_1   +  1 week    + TBD                                                                   +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 6(a)      +  4 weeks   + AWS Managed Services, Deploying and Managing AWS Resources            +
    +------------------+------------+-----------------------------------------------------------------------+

   
    Specialization 2: Microsoft Azure

    +------------------+------------+-----------------------------------------------------------------------+
    + Module 4(b)      +  4 weeks   + Microsoft Azure Building Blocks                                       +
    +------------------+------------+-----------------------------------------------------------------------+
    + Campus visit_1   +  1 week    + TBD                                                                   +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 6(b)      +  4 weeks   + Azure Managed Services, Deploying and Managing Azure Resources        +
    +------------------+------------+-----------------------------------------------------------------------+

    Specialization 3: Google Cloud Platform

    +------------------+------------+-----------------------------------------------------------------------+
    + Module 4(c)      +  4 weeks   + Google Cloud Building Blocks                                          +
    +------------------+------------+-----------------------------------------------------------------------+
    + Campus visit_1   +  1 week    + TBD                                                                   +
    +------------------+------------+-----------------------------------------------------------------------+
    + Module 6(c)      +  4 weeks   + Google Cloud Services, Deploying and Managing Google Cloud Resources  +
    +------------------+------------+-----------------------------------------------------------------------+

    Capstone Project and Campus visit

    +------------------+------------+-----------------------------------------------------------------------+
    + Project          +  8 weeks   + Capstone Project (TBD)                                                +
    +------------------+------------+-----------------------------------------------------------------------+
    + Campus visit_2   +  1 week    + Project Presentation                                                  +
    +------------------+------------+-----------------------------------------------------------------------+




  Assessment and Grading

    Attendance
    * Minimum 75% of all sessions
    * Only those who attend ≥ 75% of session duration will be awarded attendance

    Evaluation will be based on the following components:
    * Quizzes
    * Assignments
    * Case Studies
    * Modular Exams- Modular Exams after every term
    * Capstone Project

  .. csv-table::
    :header: "Assessment Type", "Total Count", "Best", "Points/Assessment", "Total points", "Weightage"
    :widths: 10, 5, 5, 10, 5, 5

    "Quizzes", 23, 20, 4, 80, 8%
    "Assignments", 27, 24, 5, 120, 12%
    "Case Studies", 2, 2, 50, 100, 10%
    "Modular Exams", 2, 2, 150, 300, 30%
    "Capstone", 1, 1, 400, 400, 40%
       , ,  , "Total", 1000, 100% 




Broad Weekly Schedule
-----------------------
    * Each module comprises of lecture sessions and Labs
    * Week starts on Saturday and ends on Sunday
    * Quiz will be based on last week session
    * Lectures are planned for 4.5 hrs on Saturday and Labs are planned for 4 hrs on Sunday
    * Labs will be based on the lecture covered on the previous day
   
    +----------------+--------------+-----------------+-------------------------------------------------------+
    + **Activity**   + **Day**      +  **Duration**   +      **Details**                                      +
    +----------------+--------------+-----------------+-------------------------------------------------------+
    + Quiz           + Saturdays    +   30 minutes    + Recap learning of previous week                       +
    +----------------+--------------+-----------------+-------------------------------------------------------+
    + Lectures       + Saturdays    +   4.5 hours     + Lecture starts on Saturday                            +
    +----------------+--------------+-----------------+-------------------------------------------------------+
    + Labs           + Sundays      +   4 hours       + Mentor-assisted tutorial sessions to complete         + 
    +                +              +                 + assignments/projects based on previous day session    +
    +----------------+--------------+-----------------+-------------------------------------------------------+    



 

.. toctree::
   :maxdepth: 1
   :caption: Module Details:
    
   Module1
   Module2
   Module3
   Module4(a)
   Module4(b)
   Module4(c)
   Module5
   Module6(a)
   Module6(b)
   Module6(c)
   Module7

