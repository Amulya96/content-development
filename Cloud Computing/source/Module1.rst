Module 1 – Overview of Cloud Services
======================================

This document captures the draft design of Module 1

Learning Outcomes

At the end of the session, the participants will be able to

    * understand the basics of cloud computing
    * understand types of delivery and deployment models
    * understand the basics of virtualization using VMware

   
Duration
    2 weeks (9 hours planned per week)


Credits
    * TBD by IIIT Delhi
    * 2 Quizzes (ungraded)
    * 2 Assignments (ungraded)

    
Instructor
    TBD

Coverage
^^^^^^^^

.. csv-table::
  :header: "Week", "Topic-Category", "Topics", "Sub-Topics & Lab assignments"
  :widths: 6, 14, 20, 40

  "Week1", "Lecture", "Evolution of Cloud", "Client/Server, Internet, Distributed Computing, Web Services"
         ,          , "Cloud Delivery Models", "Iaas, Paas, Saas"
         ,          , "Cloud Deployment Models", "Public Cloud, Private Cloud, Hybrid Cloud"
         ,          , "Core Attributes of Cloud", "Programmability, Scalability, Automation, Self Service, Pay as you Go"
         , "Labs",                        , "VMware installation, Overview of VMware"
  "Week2", "Lecture", "IaaS", "Virtualization, Popular Hypervisors, Types of Hypervisors, Virtual Machines Managers, VMs in the Public Cloud, Use cases of IaaS"
         ,          , "Paas", "Evolution of Platform, Application Lifecycle Management, IaaS vs. PaaS, Benefits of PaaS, Use cases of IaaS"
         ,          , "SaaS", "Evolution of Application Software, Defining SaaS, Benefits of SaaS, Examples of SaaS"
         , "Labs",          , "Virtualizations using VMware"

