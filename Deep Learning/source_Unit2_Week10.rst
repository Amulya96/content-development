======
Week 10
======

This document captures the resources and high-level session plan of week 10


Week 10 :: Saturday :: Session 1 :: Intro Lec :: Pre-Lecture on NLP
-------------------------------------------------------------------

Objectives
++++++++++

At the end of the session, participants will be able to

* understand NLP applications


Week 10 :: Saturday :: Session 2 ::  Intro Lec :: Non-Linear SVM
-------------------------------------------------------------------

Objectives
++++++++++

At the end of the session, participants will be able to

* visualize linearly and non-linearly separable data
* perform polynomial combinations on features

Prework
+++++++

* `Video_U2W9_00 <https://videoken.com/embed/vkene-vJFbH8_nGA>`_
  
    - In this video narrated by Prof Anoop Namboodiri, he explains about the Non-Linear feature mapping.
    - :Duration: 8 mins

* `Notes_SVM <https://cdn.iiith.talentsprint.com/AIML_V3.0/Unit2/Week9/Pre-Reading/Prof_C._V_Jawahar_Notes.pdf>`_

    - Refer to the reading material authored by the TalentSprint team to understand the math behind SVM and kernels.

* `Notes_Kernel <https://cdn.iiith.talentsprint.com/AIML_V3.0/Unit2/Week9/Pre-Reading/AIML_KER_LEC_PRM.pdf>`_

    - Refer to the reading material authored by the TalentSprint team to understand polynomial kernel.

Post-Reading
+++++++++++++

* `Notes_SVM_Kernels <https://cdn.iiith.talentsprint.com/AIML_V3.0/Unit2/Week9/Reference_Material/AIML_SVM-NL_LEC_ABS.pdf>`_

    - Refer to the reference material authored by the TalentSprint team visually depicts how SVM deals with non-linear data.


Session Plan
++++++++++++

+-----------------------------------------------------------------------------------+----------+
| Non-Linear decision boundaries                                                    | 20 mins  |
+-----------------------------------------------------------------------------------+----------+
| Polynomial kernel Example                                                         | 25 mins  |
+-----------------------------------------------------------------------------------+----------+
| Demo Experiment: Non-linear Decision boundary for cows and wolves dataset         | 45 mins  |
+-----------------------------------------------------------------------------------+----------+


Individual Labs
+++++++++++++++

:Experiment id: EIL_330

:Objectives:

    At the end of the experiment, participants will be able to

    - perform SVM classifier using different kernels

:Dataset: Bank Notes Dataset
:Colab Notebook: `Notebook_U2W9_00 <https://drive.google.com/file/d/1Q23rAn4kF7xjveXyZMwMto2RvvxMclCV/view?usp=sharing>`_

|

:Experiment id: EIL_340

:Objectives:

    At the end of the experiment, participants will be able to

    - extract meaningful features from the images using PCA
    - apply SVM on the extracted data and recognize the face

:Dataset: Labeled Faces in the Wild from Sklearn

:Colab Notebook: `Notebook_U2W9_10 <https://drive.google.com/file/d/1BnuOIz0qfqN0rIE5ia6qsuwZcGXa8FUC/view?usp=sharing>`_


Week 10 :: Sunday:: Session 1 :: Intro to NLP
--------------------------------------------------

Objectives
++++++++++

At the end of the session, participants will be able to

* understand the importance of NLP


Week 10 :: Sunday :: Session 1 :: Non-Linear SVM
-------------------------------------------------

Objectives
++++++++++

At the end of the session, participants will be able to

* understand the limitations of non linear SVMs
* understand how a feature map converts a linearly non-separable problem into a separable problem
* explore the types of Kernels


Session Plan
++++++++++++

+-----------------------------------------------------------------------------------+----------+
| (Recap) Linear Classifiers                                                        | 20 mins  |
+-----------------------------------------------------------------------------------+----------+
| SVM Formulation                                                                   | 20 mins  |
+-----------------------------------------------------------------------------------+----------+
| Non-Linear Feature Mapping                                                        | 20 mins  |
+-----------------------------------------------------------------------------------+----------+
| Kernel Strategy                                                                   | 30 mins  |
+-----------------------------------------------------------------------------------+----------+



Faculty Resources
-----------------

:Intro.Deck: `Deep Learning SVM <https://iiitaphyd-my.sharepoint.com/:p:/g/personal/aim_execprogl_iiit_ac_in/EQgd3Uo6ax9Ps6S67RNb0RsBPK8wicyZ_6C1B-yfsRndcA?e=wIaIQn>`_

:Intro.Deck: `Pre Lecture of NLP <https://iiitaphyd-my.sharepoint.com/:p:/g/personal/aim_execprogl_iiit_ac_in/EdiwJVoZBZBLkTtK38zmBJYBXRCyfayWOPBOpXaD6iv1yA?e=AgysQx>`_

:Deck: `Non-Linear SVM <https://iiitaphyd-my.sharepoint.com/:p:/g/personal/aim_execprogl_iiit_ac_in/Ecbb57EiZOBGlVj7bDRxDBUBO9N_ktrcRhdzQIrzwbAcEA?e=WprwG7>`_

:Deck: `NLP <https://iiitaphyd-my.sharepoint.com/:p:/g/personal/aim_execprogl_iiit_ac_in/EQZ8dbKCTq5Kt6yeZemcD6kBUHc8BVk3qxX2s19ehoYI_w?e=zu6mTw>`_


:Demo Problems: `Demo_Non-Linear_SVM_RBF <https://drive.google.com/file/d/1lODxzbUKjonByyate-tAOTMag2O47N2R/view?usp=sharing>`_

:Demo Problems: `Demo_Non_Linear_SVM_multi-class <https://drive.google.com/file/d/12GI9R_m6bIEWYCi3bg5sRFoU3c-ZwMgD/view?usp=sharing>`_

:Question Bank: `CFP_U2W10_08 <https://cdn.iiith.talentsprint.com/AIML_V3.0/Batch_16/Unit2/Week10/CFP_U2W10_08.pdf>`_

:Question Bank: `CFU_U2W10_08 <https://cdn.iiith.talentsprint.com/AIML_V3.0/Batch_16/Unit2/Week10/CFU_U2W10_08.pdf>`_

:Question Bank: `Weekly Test-4 <https://cdn.iiith.talentsprint.com/AIML_V3.0/Batch_16/Unit2/Week10/Weekly_Test_4.pdf>`_


Discussing about git


